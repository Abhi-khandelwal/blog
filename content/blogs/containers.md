---
title: "How Containers Came Into Existence ?"
date: 2020-07-01T00:24:38+05:30
draft: false
share: true
tags: ["docker", "devops", "VMs"]
---

In this blog, I will discuss the concepts of Containers, but first we have to get an understanding of when this term came into existence and how it made a developer’s lives easier.

For that, we first have to understand that the precursor of Containers, why there was the need to switch to containers, and discuss the architecture of Virtual Machines and Containers.

Although the goal of both terms are similar (i.e. isolate an application and its dependencies into a self-contained environment that can run anywhere). It means it can be run on your local machine, your co-worker’s machine, or a cloud provider’s service such as AWS.

The primary difference between Virtual Machines and Containers is their architectural approach to virtualization of an OS.

Now, we are going to deep dive into their individual architectural approach.

## Virtual Machines

Virtual Machines basically provide us the ability to emulate separate OS. This means that it allows us to run an OS within another OS.

Suppose your system are using a Windows OS, and you want to run a Linux OS on your system. In that case, a virtual machine comes into play. In fact, more than one VMs can live within a single physical server.

Let’s discuss its architecture:

[![Virtual Machine Architecture](/img/virtual_machine_architecture.jpeg "Virtual Machine Architecture")]()

In the above figure, there are 3 VMs running on top of the Hypervisor. These VMs that are running on host OS (**using hypervisor**) are also called as “guest” OS. These guest OS’s contain all dependencies and needs of the application. Host machines also provide resources (**again using hypervisor**) such as RAM and CPU to the virtual machines. These resources can be divided according to the needs of applications.

Lets make it more simple: A **hypervisor** is a software/firmware used to share resources of host OS to guest OS, there are two types of hypervisors.
   - Bare-Metal Hypervisor.
      This type of Hypervisor interface directly with hardware, it doesn’t need a host OS to run on.


[![Bare Metal Hypervisor](/img/bare_metal_hypervisor.jpeg "Bare Metal Hypervisor")]()

    
   - Hosted Hypervisor
       This type of Hypervisor is a software that runs on host OS.



[![Hosted Hypervisor](/img/hosted_hypervisor.jpeg "Hosted Hypervisor")]()



As we know that in our modern world, our applications can take up a lot of resources, and this quickly increase the usage of RAM and CPU, which is not economical which led developers to switch from Virtual Machines to containers.

## Containers

Containers also provide a way to isolate the application and provide a virtual platform for applications to run. So how is it different from Virtual Machines?

[![How](/img/confusion.gif "How?")]()

   - Virtual Machines provide **Physical/Hardware level** virtualization but Containers provide **OS level** virtualization also known as containerization. This allows us to handle multiple isolated platforms that don’t use the host device’s physical resources. This is a more effective approach in terms of memory and CPU usage.
    
   - Containers share the host’s system kernel with other containers.

Let's discuss these two points.

[![How](/img/containers_architecture.jpeg "How?")]()

I think this figure lacks something about the function of a hypervisor, there are multiple containers running on a single Container engine. The sharing of kernel space (OS) with other containers makes containers very lightweight compared to Virtual Machines.

In general, Containers are managed by a **Container** Engine which is responsible for allocating resources to running containers by communicating with the kernel. Container-based virtualization uses the kernel of the host operating system to run guest instances, also known as Operating System Level Virtualization. Since Containers doesn’t consist of guest a OS, this means that container doesn’t require its own operating system. It uses fewer resources and consumes only the resources required for the application that is run upon starting the container.

As we discussed earlier, containers share the host system kernel with others. Sharing OS resources such as libraries significantly reduces the need to reproduce the operating system code, and this means that a server can run multiple workloads with a single operating system installation.

The benefits of applications of Containers over Virtual Machines are:

   - Small in size
   - They share the host system kernel
   - Start much faster
   - They use a fraction of the memory compared to booting an entire OS

But just because containers are extremely popular it doesn’t mean virtual machines are out of the date. If your company’s first priority is security then you are also going to want to stay with **VMs for now**.

In the real world, I expect most of us are going to be running both containers and VMs on our clouds and data-centers. The economy of containers at scale makes too much financial sense for anyone to ignore.

Thanks for reading, hope you liked it. :)