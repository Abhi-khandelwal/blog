+++
title = "About me"
date = 2020-07-01T00:20:00+05:30
draft = false
paginate = false
comments = false
share = false
type = "about"
+++


Hi I'm **Abhi Khandelwal**,

A passionate innovator and software engineer who aim to build impactful products, develop long-term roadmaps and drive them to completion.
I'm always looking for an opportunity to work in new and cutting edge technologies and develop my skillsets.

I am skilled in following technologies:
   - **Languages:**
      Python, GoLang, Javascript, C++, PHP
   - **Frameworks:**
   	  Django, Flask, ReactJs
   - **Others:**
      MySQL, ORM, gRPC, REST, MVC, Redis, Docker, Protocol Buffers